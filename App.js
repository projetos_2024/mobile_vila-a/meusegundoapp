import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import Layout from './layoutDeTelaEstrutura';
import LayoutHorizontal from './layoutHorizontal';
import LayoutGrade from './layoutGrade';
import Componentes from "./componets"

export default function App() {
  return (
    <View style={styles.container}>
      <Componentes></Componentes>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  
});
